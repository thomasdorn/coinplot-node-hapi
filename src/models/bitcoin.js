const mongoose = require('../services/mongoose');


const bitcoinPriceSchema = new mongoose.Schema({
    coinname: { type: String},
    symbol: { type: String },
    quotes: { type: Object },
    last_updated: { type: Number, index: true },
    count: { type: Number, index: true }
  },
  {
    timestamps : true
  },
  {
    toObject: {
      transform: function (doc, ret) {
        delete ret._id;
      }
    },
    toJSON: {
      transform: function (doc, ret) {
      }
    }
  });

let model = mongoose.model('bitcoinPrice', bitcoinPriceSchema);

module.exports = model;
