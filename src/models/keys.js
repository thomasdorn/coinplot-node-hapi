const mongoose = require('../services/mongoose');


const keysSchema = new mongoose.Schema({

  username: { type: String, index:true },
  keypassword: { type: String, index:true },
  ethereumKey: { type: String, index:true}

});

let model = mongoose.model('keys', keysSchema);

module.exports = model;
