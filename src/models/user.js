const mongoose = require('../services/mongoose');


const userSchema = new mongoose.Schema({
    username: { type: String, unique:true, index:true },
    passwordMd5: String,
    hawkId: { type: String, unique:true, index:true },
    hawkKey: String,
    isLoggedIn : Boolean,
    usernameMd5 : String,
    position: Object
  },
  {
    timestamps : true
  },
  {
    toObject: {
      transform: function (doc, ret) {
        delete ret._id;
      }
  },
    toJSON: {
      transform: function (doc, ret) {
      }
    }
  });

let model = mongoose.model('users', userSchema);

module.exports = model;
