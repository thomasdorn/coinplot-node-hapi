const mongoose = require('../services/mongoose');


const ethereumTxSchema = new mongoose.Schema({
    username: {type: String},
    uuid: {type: String},
    hash: { type: Object },
    receipt : { type: Object },
    confirmationNumber : { type: Object },
    error : { type: Object}
  },
  {
    timestamps : true
  },
  {
    toObject: {
      transform: function (doc, ret) {
        delete ret._id;
      }
    },
    toJSON: {
      transform: function (doc, ret) {
      }
    }
  });

let model = mongoose.model('ethereumTx', ethereumTxSchema);

module.exports = model;
