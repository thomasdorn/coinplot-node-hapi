let MarkDown = require('../utils/markdown');

class SwaggerHandler {
  static index(){
    return {
      handler: (request, h) => {
        // return {test:"hello world"};
        return h.view('swagger.html', {
          title: 'examples/ejs/templates/basic | Hapi ' + request.server.version,
          message: 'Hello Ejs!',
          passwords: this.getKeySecretData()
        });
      }
    }
  }
  static getKeySecretData(){
    let passwords = { keySecret: {} };

      passwords.keySecret.u = 'xb9PSUscLi6G9D4Afdg9cf1Bbj0=';
      passwords.keySecret.p = 'werxhqb98rpaxn39848xrunpaw3489ruxnpa98w4rxn';

    return passwords;
  }
}

module.exports = SwaggerHandler;
