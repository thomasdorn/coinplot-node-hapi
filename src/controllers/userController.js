const generate = require('iota-generate-seed');
let crypto = require('crypto');
let userModel = require('../models/user');
let Boom = require('boom');
let Web3 = require('web3');
let Net = require('net');
let Joi = require('joi');
let secretKeys = require('../services/secret');
let redis = require('../services/redis');
const openpgp = require('openpgp');
let CommonEncryption = require('../providers/encryptionProvider');
let EthereumController = require('../controllers/ethereumController');
let Response = require('../providers/response');
let Logger = require('../services/logger');
const JWT   = require('jsonwebtoken');
let socket = require('socket.io-client')('https://http2.coinplot.info');
const format = require('../providers/formatters');
const axios = require('axios');


let logInfo = function(info) {
  Logger.setModuleAndLog(module, info);
};


class UserController {
  constructor(redis) {
    this.redis = redis;
  }
  createUser() {
    return {
      description: 'Create a user',
      notes: 'Creates a user and returns the hawk id and key of the user. They cannot be set to string will return invalid payload.' +
        ' No auth required',
      tags: ['api', 'User'],
      validate: {
          payload : {
            username: Joi.string().trim().required().disallow('string'),
            password: Joi.string().trim().required().disallow('string'),
          }
      },
      handler: (request, h) => {
        let username = request.payload.username;
        let password = request.payload.password;
        let query = {
          username:username
        };
        return userModel.find(query).exec().then(results=>{
          if (results.length===0) {
            let passwordMd5 = crypto.createHash('md5').update(password).digest("hex");
            let hawkId = generateHash();
            let hawkKey = generateHash();
            let doc = {
              username: username,
              passwordMd5: passwordMd5,
              hawkId: hawkId,
              hawkKey: hawkKey
            };
            return new userModel(doc).save().then(result => {
              let formattedResult = result.toJSON();
              delete formattedResult['passwordMd5'];
              delete formattedResult['createdAt'];
              delete formattedResult['updatedAt'];
              delete formattedResult['__v'];
              return formattedResult;
            });
          }else {
            return Boom.badRequest('user name taken');
          }
        });
      }
    }
  }


  loginUser() {
    return {
      description: 'Login a user',
      notes: 'Creates a user and returns the hawk id and key of the user. They cannot be set to string will return invalid payload.' +
        ' No auth required',
      tags: ['api', 'User'],
      validate: {
        query: {
          setsecret: Joi.boolean()
        },
        payload: {
          username: Joi.string().trim().required().disallow('string'),
          password: Joi.string().trim().required().disallow('string'),
          position: Joi.any()
        }
      },
      handler: (request, h) => {
        let secret = secretKeys.getJwtSecretKey();
        let username = request.payload.username;
        let password = request.payload.password;
        let position = request.payload.position;
        let setSecret = request.query.setsecret;
        let query = {
          username: username,
          passwordMd5: crypto.createHash('md5').update(password).digest("hex")
        };
        return this.countLoggedInUsers().then(usersLoggedIn => {
          if (usersLoggedIn === 0) {
          } else {
            console.log({'users logged in': usersLoggedIn});
          }
          // return axios.get('http://dornhost.com:7000/clearusernamerabbitmq/' + username).then((resp) => {
          //   console.log(resp.data);
            return userModel.find(query).exec().then(results => {
              if (results.length) {
                let result = results instanceof Array ? results.pop() : results;
                result.hawkKey = generateHash();
                result.isLoggedIn = true;
                result.position = position;
                if (!result.usernameMd5) {
                  result.usernameMd5 = crypto.createHash('md5').update(username).digest("hex");
                  redis.client.set(username + '_Md5', result.usernameMd5);
                } else {
                  redis.client.set(username + '_Md5', result.usernameMd5);
                }
                return userModel.findOne(query).exec().then(cursor => {
                  cursor = result;
                  cursor.save();
                  const obj = {id: result._id, username: result.username};
                  const token = JWT.sign(obj, secret, {
                    expiresIn: '1d'
                  });
                  if (setSecret === true) {
                    let hmacSecretKey = 'AWKGXVXGQTBEDPDEHOGV9EZINVZYLKTSELCFZ9ODPW';
                    redis.setSecretKey(username, hmacSecretKey);
                  }
                  return {
                    id: result._id,
                    username: result.username,
                    hawkId: result.hawkId,
                    hawkKey: result.hawkKey,
                    jwt: token,
                    usernameMd5: result.usernameMd5
                  }
                })
              } else {
                let query = {
                  username: username
                };
                return userModel.find(query).exec().then(results => {
                  if (results.length) {
                    return Promise.reject(new Error('invalid password'))
                  } else {
                    return Promise.reject(new Error('no user found'));
                  }
                });
              }
            })
          // })
        }).catch(async e => {
          logInfo(e);
          return await Response.formatResponseSuccessDataOrError(false, false, e.message.toString(), username);
        });
      }
    }
  }
  acceptClientSecret() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Login a user',
      notes: 'Creates a user and returns the hawk id and key of the user. They cannot be set to string will return invalid payload.' +
        ' No auth required',
      tags: ['api', 'User'],
      validate: {
        payload : {
          hmacsecretkey: Joi.string().trim().required().disallow('string'),
        },
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        let username = await EthereumController.getUsernameFromHawk(request);
        let hmacSecretKey = request.payload.hmacsecretkey;
        let privateKey = (await this.redis.getEncryptionKeys()).privateKeyArmored;
        const privKeyObj = (await openpgp.key.readArmored(privateKey)).keys[0];
        await privKeyObj.decrypt(await this.redis.getPrivateKeyPassword());
        const options = {
          message: await openpgp.message.readArmored(hmacSecretKey),    // parse armored message
          privateKeys: [privKeyObj]                                 // for decryption
        };
        const decrypted = await openpgp.decrypt(options);
        const plainText = await openpgp.stream.readToEnd(decrypted.data);
        await this.redis.setSecretKey(username, plainText);
        logInfo({'accepted secret key: ': plainText});
        return Response.formatResponseSuccessDataOrError(true, false, {status:'accepted secret key'}, username);
      }
    }
  }
  acceptClientSecretUnencrypted() {
    return {
      auth: {
        strategy: "jwt"
      },
      description: 'Login a user',
      notes: 'Creates a user and returns the hawk id and key of the user. They cannot be set to string will return invalid payload.' +
        ' No auth required',
      tags: ['api', 'User'],
      validate: {
        payload : {
          hmacsecretkey: Joi.string().trim().required().disallow('string'),
        },
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        let username = await EthereumController.getUsernameFromHawk(request);
        let hmacSecretKey = request.payload.hmacsecretkey;
        await redis.setSecretKey(username, hmacSecretKey);
        return Response.formatResponseSuccessDataOrError(true, false, {status:'accepted secret key'}, username);
      }
    }
  }
  getServerPublicKey() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Login a user',
      notes: 'Creates a user and returns the hawk id and key of the user. They cannot be set to string will return invalid payload.' +
        ' No auth required',
      tags: ['api', 'Encryption'],
      validate: {
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        return (await this.redis.getEncryptionKeys()).publicKeyArmored;
      }
    }
  }
  logoutUser() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Login a user',
      notes: 'Creates a user and returns the hawk id and key of the user. They cannot be set to string will return invalid payload.' +
        ' No auth required',
      tags: ['api', 'User'],
      validate: {
        query: {
          token: Joi.string(),
          services: Joi.boolean()
        }
      },
      handler: async (request, h) => {
        logInfo({status:'inside logout user'});
        let username = await this.getUsernameFromHawk(request);
        try {
          logInfo(username);
          let query = {
            username:username
          };
          let cursor = await userModel.findOne(query).exec();
          cursor.hawkKey = generateHash();
          cursor.isLoggedIn = false;
          await cursor.save();
          let currentUsers = await this.countLoggedInUsers();
          if (currentUsers === 0 && request.query.services === false) {
            // axios.get('http://ethbackend:7000/stop').then()
            //   .catch(err=>console.log({'could not connect':err.message}));
            // axios.get('http://ethbackend:7000/stopbtc').then()
            //   .catch(err=>console.log({'could not connect':err.message}));
          }
          return await this.formatResponseSuccessDataOrError(true, false, {status:'user has been logged out'}, username);
        }catch (e) {
          logInfo(e);
          return await this.formatResponseSuccessDataOrError(false, false, {status:'error logging out'}, username);
        } finally {
        }
      }
    }
  }
  async countLoggedInUsers() {
    return await userModel.find({$and: [{isLoggedIn: {$exists: true}}, {isLoggedIn: true}]}).countDocuments();
  }
  async getUsernameFromHawk(request) {
    let hawkKey = request.auth.credentials.hawk.key;
    let query = {
      hawkKey:hawkKey
    };
    let result = await userModel.findOne(query).exec();
    return result.username;
  }

  async formatResponseSuccessDataOrError(success, encrypted , result, username) {
    if (success === true) {
      if (encrypted===true) {
        let secretKey = await redis.getSecretKey(username);
        let encryptedBody = CommonEncryption.encrypt(result, secretKey);
        const hmac = await this.generateHmac(encryptedBody, username);
        return await this.formatResponse(true, encryptedBody , hmac, username)
      }else {
        const hmac = await this.generateHmac(result, username);
        return await this.formatResponse(false, result, hmac , username);
      }
    }else if(success === false){
      return this.formatFailedResponse(result);
    }else {
      return new Error("No result To format");
    }
  }
  async formatFailedResponse(error) {
    return {
      success:false,
      error: error
    }
  }
  async formatResponse(encrypted , data, hmac, username) {
    if(encrypted===true) {
      return {
        success: true,
        encrypted: true,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }else {
      return {
        success: true,
        encrypted:false,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }
  }
  async generateHmac(body, username) {
    let secretKey = await redis.getSecretKey(username);
    return crypto.createHmac('sha1', secretKey).update(JSON.stringify(body)).digest('hex')
  }

}
function formatEmit(username, data) {
  return {username,data};
}
function generateHash() {
  let offset = (new Date).getTime()*Math.random();
  let iotaSeed = generate();
  let toHash = (offset+iotaSeed).shuffle();
  let sha = crypto.createHash('sha1').update(toHash);
  return sha.digest('base64');
}

String.prototype.shuffle = function () {
  let a = this.split(""),
    n = a.length;
  for(let i = n - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }
  return a.join("");
};

module.exports = new UserController(redis);
