let CommonEncryption = require('../providers/encryptionProvider');
let crypto = require('crypto');
let Joi = require('joi');
let userModel = require('../models/user');
let redis = require('../services/redis');
let Logger = require('../services/logger');

let logInfo = function (info) {
  Logger.setModuleAndLog(module, info);
};

class EncryptionController {
  static encryptDecryptBody() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Encrypts a post body with the users secret key',
      notes: 'encrypted true will encrypt the data encrypt with false will decrypt the data',
      tags: ['api', 'Ethereum'],
      validate: {
        payload: {
          data: Joi.string().trim().required().disallow("string").disallow(""),
          encrypted: Joi.boolean().required(),
          hmac: Joi.string().trim()
        },
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        let data = request.payload.data;
        let encrypted = request.payload.encrypted;
        let username = await this.getUsernameFromHawk(request);
        if (encrypted){
          logInfo('inside date');
          return this.formatResponseSuccessDataOrError(true, true, data, username);
        }else {
          data = await this.decrypt(request);
          logInfo(data);
          return this.formatResponseSuccessDataOrError(true, false, data, username);
        }
      }
    }
  }
  static async encrypt(request) {
    try {
      let encrypted = request.payload.encrypted;
      let data = request.payload.data;
      let username = await this.getUsernameFromHawk(request);
      let secretKey = await redis.getSecretKey(username);
      if (this.checkHmac(request, username)) {
        logInfo({'hmac for create key match': true});
        if (encrypted === true) {
          return CommonEncryption.decrypt(data, secretKey);
        }
      }else {
        logInfo({'hmac for create key match': false});
      }
    }catch (e) {
      return e;
    }
  }
  static async decrypt(request) {
    try {
      let data = request.payload.data;
      let username = await this.getUsernameFromHawk(request);
      let secretKey = await redis.getSecretKey(username);
      if (this.checkHmac(request, username)) {
        logInfo({'hmac for encrypt data...': true});
        return CommonEncryption.encrypt(data, secretKey);
      }else {
        logInfo({'hmac for encrypt data...': false});
      }
    }catch (e) {
      return e;
    }
  }
  static async formatResponseSuccessDataOrError(success, encrypted , result, username) {
    logInfo({'formatting response...' :
        {
          success: success,
          encrypted: encrypted,
          result: result,
          username: username
        }
      });
    if (success === true) {
      if (encrypted === true) {
        let secretKey = await redis.getSecretKey(username);
        let encryptedBody = CommonEncryption.encrypt(result, secretKey);
        const hmac = await this.generateHmac(encryptedBody, username);
        return await this.formatResponse(true, encryptedBody , hmac, username)
      }else {
        const hmac = await this.generateHmac(result, username);
        return await this.formatResponse(false, result, hmac , username);
      }
    }else if (success === false) {
      return this.formatFailedResponse(result);
    }else {
      return new Error("No result To format");
    }
  }
  static async formatFailedResponse(error) {
    return {
      success:false,
      error: error
    }
  }
  static async formatResponse(encrypted , data, hmac, username) {
    if(encrypted===true) {
      return {
        success: true,
        encrypted: true,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }else {
      return {
        success: true,
        encrypted:false,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }
  }
  static async checkHmac(request, username) {
    let hmac = request.payload.hmac;
    let secretKey = await redis.getSecretKey(username);
    let requestPayloadCopy = JSON.parse(JSON.stringify(request.payload));
    delete requestPayloadCopy.hmac;
    let testSig = crypto.createHmac('sha1', secretKey).update(JSON.stringify(requestPayloadCopy)).digest('hex');
    return testSig !== hmac;
  }
  static async generateHmac(body, username) {
    logInfo({'generating hmac...' : {
      data:body,
        username:username
      }});
    let secretKey = await redis.getSecretKey(username);
    return crypto.createHmac('sha1', secretKey).update(JSON.stringify(body)).digest('hex')
  }
  static async getUsernameFromHawk(request) {
    let hawkKey = request.auth.credentials.hawk.key;
    let query = {
      hawkKey:hawkKey
    };
    let result = await userModel.findOne(query).exec();
    return result.username;
  }
}

module.exports = EncryptionController;
