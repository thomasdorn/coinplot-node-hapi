let Joi = require('joi');
let redis = require('../services/redis');
let crypto = require('crypto');

class TestingController {
  static helloWorld() {
    return {
      description: 'Test Hawk',
      notes: 'This is a test for hawk you will need a valid hawkId key',
      tags: ['api', 'Testing'],
      handler: (request, h) => {
        return {test:"hello world"};
      }
    }
  }
  static testHawk() {
    return {
      auth: {
        strategy: "hawk"
      },
      description: 'Test Hawk',
      notes: 'This is a test for hawk you will need a valid hawkId key',
      tags: ['api', 'Testing'],
      handler: (request, h) => {
        return {test:"hello world"};
      }
    }
  }
  static testHawkJwt() {
    return {
      auth: {
        strategies: ["hawkjwt"]

      },
      description: 'Test Hawk',
      notes: 'This is a test for hawk you will need a valid hawkId key',
      tags: ['api', 'Testing'],
      validate: {
        query: {
          token: Joi.string()
        }
      },
      handler: (request, h) => {
        return {test:"hello world"};
      }
    }
  }
  static getHmac() {
    return {
      auth: {
        strategies: ["hawkjwt"]
      },
      description: 'Generate an HMAC for a post body',
      notes: 'you will need to be loged in and already have established a secret key with the set secret api',
      tags: ['api', 'Testing'],
      validate: {
        payload: {
          username: Joi.string().required(),
          payload: Joi.object().required()
        },
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        let secretKey = await redis.getSecretKey(request.payload.username);
        return crypto.createHmac('sha1', secretKey).update(JSON.stringify(request.payload.payload)).digest('hex');
      }
    }
  }
}

module.exports = TestingController;
