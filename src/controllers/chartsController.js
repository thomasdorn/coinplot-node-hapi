let Boom = require('boom');
let Joi = require('joi');
let _ = require('lodash');
let userModel = require('../models/user');
const format = require('../providers/formatters');
let redis = require('../services/redis');
let crypto = require('crypto');
let CommonEncryption = require('../providers/encryptionProvider');
let Logger = require('../services/logger');
let axios = require('axios');
let uuid = require('uuid');
let socket = require('socket.io-client')('https://http2.coinplot.info');
const BtcClient = require('bitcoin-core');

let btcpriceModel = require('../models/bitcoin');
let ethpriceModel = require('../models/ethereum');

let logInfo = function (info) {
  Logger.setModuleAndLog(module, info);
};

class ChartsController {
  constructor(testval) {
    this.testval = testval;
  }
  getBtcPrice() {
    return {
      description: 'Creates an ethereum key',
      notes: 'Creates an ethereum key by connecting to the geth ipc. User name and a key password are required and ' +
        'cannot be left blank or set to string. A valid user is required in the database and more then 1 key can be created' +
        ' for a user. Requires a valid hawk id and key',
      tags: ['api', 'Bitcoin'],
      validate: {
        query: {
          token: Joi.string(),
          number: Joi.number()
        }
      },
      handler: async (request, h) => {
        let coinplot = {};
        coinplot.locals = request.query;
        let response = {
          prices: [],
          dates: []
        };
        try {
          this.testval = this.testval + 1;
          console.log("getting btc price data Num class queries / core: " + this.testval + " Num of docs fetched: "
            + coinplot.locals.number);
          let results = await btcpriceModel.find({}).sort({last_updated:-1}).limit(coinplot.locals.number).exec();
          results = results.reverse();
          for (let item of results) {
            response.prices.push(item.quotes.price);
            response.dates.push(item.createdAt);
          }
          return await this.formatResponseSuccessDataOrError(true, false, response, null);
        }catch (e) {
          return await this.formatResponseSuccessDataOrError(
            false, false,
            Boom.badRequest(e), null)
        }

      }
    }
  }
  getEthPrice() {
    return {
      description: 'Creates an ethereum key',
      notes: 'Creates an ethereum key by connecting to the geth ipc. User name and a key password are required and ' +
        'cannot be left blank or set to string. A valid user is required in the database and more then 1 key can be created' +
        ' for a user. Requires a valid hawk id and key',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string(),
          number: Joi.number()
        }
      },
      handler: async (request, h) => {
        let coinplot = {};
        coinplot.locals = request.query;
        let response = {
          prices: [],
          dates: []
        };
        try {
          this.testval = this.testval + 1;
          console.log("getting eth price data Num class queries: " + this.testval + " - Num of docs fetched: "
            + coinplot.locals.number);
          let results = await ethpriceModel.find({}).sort({last_updated:-1}).limit(coinplot.locals.number).exec();
          results = results.reverse();
          for (let item of results) {
            response.prices.push(item.quotes.price);
            response.dates.push(item.createdAt);
          }
          return await this.formatResponseSuccessDataOrError(true, false, response, null);
        }catch (e) {
          return await this.formatResponseSuccessDataOrError(
            false, false,
            Boom.badRequest(e), null)
        }

      }
    }
  }
  async postDecrypt(request) {
    try {
      let encrypted = request.payload.encrypted;
      let data = request.payload.data;
      let username = await this.getUsernameFromHawk(request);
      let secretKey = await redis.getSecretKey(username);
      if (this.checkHmac(request, username)) {
        console.log({'hmac for create key match': true});
        if (encrypted === true) {
          return CommonEncryption.decrypt(data, secretKey);
        }
      }
    }catch (e) {
      return e
    }
  }


  async getUsernameFromHawk(request) {
    let hawkKey = request.auth.credentials.hawk.key;
    let query = {
      hawkKey:hawkKey
    };
    let result = await userModel.findOne(query).exec();
    return result.username;
  }
  async checkHmac(request, username) {
    let hmac = request.payload.hmac;
    let secretKey = await redis.getSecretKey(username);
    let requestPayloadCopy = JSON.parse(JSON.stringify(request.payload));
    delete requestPayloadCopy.hmac;
    let testSig = crypto.createHmac('sha1', secretKey).update(JSON.stringify(requestPayloadCopy)).digest('hex');
    if (testSig!==hmac){
      return true;
    }else {
      return false;
    }
  }
  async formatResponseSuccessDataOrError(success, encrypted , result, username) {
    if (success === true) {
      if (encrypted===true) {
        let secretKey = await redis.getSecretKey(username);
        let encryptedBody = CommonEncryption.encrypt(result, secretKey);
        const hmac = await this.generateHmac(encryptedBody, username);
        return await this.formatResponse(true, encryptedBody , hmac, username)
      }else if (username===null) {
        return await this.formatResponse(false, result, null, username);
      } else {
        const hmac = await this.generateHmac(result, username);
        return await this.formatResponse(false, result, hmac , username);
      }
    } else if(success === false){
      return this.formatFailedResponse(result);
    } else {
      return new Error("No result To format");
    }
  }
  async formatFailedResponse(error) {
    return {
      success:false,
      error: error
    }
  }
  async formatResponse(encrypted , data, hmac, username) {
    if(encrypted===true) {
      return {
        success: true,
        encrypted: true,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }else {
      return {
        success: true,
        encrypted:false,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }
  }
  async formatResponseLock(data) {
    return lock.acquire(this.username, async ()=>{
      return await this.generateHmac(data, this.username)
    }).then(hmac=>{
      return {
        success: true,
        data: data,
        hmac: hmac
      }
    });
  }
  async generateHmac(body, username) {
    let secretKey = await redis.getSecretKey(username);
    return crypto.createHmac('sha1', secretKey).update(JSON.stringify(body)).digest('hex')
  }
}

module.exports = new ChartsController(0);
