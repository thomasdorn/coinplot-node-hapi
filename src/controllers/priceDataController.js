let Boom = require('boom');
let Joi = require('joi');
let _ = require('lodash');
let pricedataprovider = require('../providers/price_data_provider');

class PriceDataController {
  static gethMessage() {
    return {
      plugins: {
        websocket: {only: true, autoping: 30 * 1000},
      },
      handler: (request, h) => {
        console.log(request);
          return {at: "baz", seen: request.payload}
        }
      }
    }
  static getBitcoinPrice(){
    return {
      description: 'Get Bitcoin Price',
      tags: ['api', 'Quotes'],
      validate: {

      },
      handler: (request, reply) => {
        return pricedataprovider.getPriceData('bitcoin').then(value=>{
          return value;
        })
      },
    }
  }
  static getEthereumPrice(){
    return {

      description: 'Get Ethereum Price',
      tags: ['api', 'Quotes'],
      validate: {

      },
      handler: (request, reply) => {
        return pricedataprovider.getPriceData('ethereum').then(value=>{
          return value;
        })
      },
    }
  }
  static getZcashPrice(){
    return {

      description: 'Get Zcash Price',
      tags: ['api', 'Quotes'],
      validate: {

      },
      handler: (request, reply) => {
        return pricedataprovider.getPriceData('zcash').then(value=>{
          return value;
        }).catch(e=>{

        })
      },
    }
  }
}

module.exports = PriceDataController;
