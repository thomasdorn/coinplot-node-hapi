let Boom = require('boom');
let Joi = require('joi');
let _ = require('lodash');
let Web3 = require('web3');
let Net = require('net');
let keysModel = require('../models/keys');
let userModel = require('../models/user');
let ethereumTxModel = require('../models/ethereumTx');
const format = require('../providers/formatters');
let redis = require('../services/redis');
let crypto = require('crypto');
let CommonEncryption = require('../providers/encryptionProvider');
let Logger = require('../services/logger');
let axios = require('axios');
let ObjectId = require('../services/mongoose').Types.ObjectId;
let uuid = require('uuid');
let socket = require('socket.io-client')('https://http2.coinplot.info');
const net = require('net');
const rabbit = require('../providers/rabbitmq');
let logInfo = function (info) {
  Logger.setModuleAndLog(module, info);
};

class EthereumController {
  static runJarFile() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Creates an ethereum key',
      notes: 'Creates an ethereum key by connecting to the geth ipc. User name and a key password are required and ' +
          'cannot be left blank or set to string. A valid user is required in the database and more then 1 key can be created' +
          ' for a user. Requires a valid hawk id and key',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string(),
          value: Joi.number()
        }
      },
      handler: async (request, h) => {
          let value = request.query.value;
          let username = await this.getUsernameFromHawk(request);
          try {
            await rabbit.sendMasterChannelRabbit({
              username,
              workers: ["coinplot_python_worker_1"],
              digits: value,
              command: 'runJarFile'
            });
            let result = await rabbit.getUserMessageFromRabbit(username);
            return await this.formatResponseSuccessDataOrError(true, false, result, username);
          } catch (e) {
            logInfo(e);
            return await this.formatResponseSuccessDataOrError(false, false,
                Boom.badRequest(e), null)
          }
        }
      }
    }
  static getEthereumKeyFile() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Creates an ethereum key',
      notes: 'Creates an ethereum key by connecting to the geth ipc. User name and a key password are required and ' +
        'cannot be left blank or set to string. A valid user is required in the database and more then 1 key can be created' +
        ' for a user. Requires a valid hawk id and key',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        try {
          let balances = [];
          let username = await this.getUsernameFromHawk(request);
          const web3 = new Web3('ws://ethbackend:8546');
          logInfo({'balance requested from':username});
          let results = await keysModel.find({username:username}).exec();
          let accounts = await web3.eth.getAccounts();
          for (let item of results) {
            logInfo({'ethereum key results':item});
            logInfo(await web3.eth.getBalance(item));
            balances.push({
              ethereumKey: item.ethereumKey,
              balance: web3.utils.fromWei(await web3.eth.getBalance(item.ethereumKey))
            })
          }
          logInfo({'response':balances});
          return this.formatResponseSuccessDataOrError(true, false, balances, username);
        } catch (e) {
          logInfo(e);
          return await this.formatResponseSuccessDataOrError(false, false,
            Boom.badRequest(e), null)
        }

      }
    }
  }

  static checkBalanceUnencrypted() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Creates an ethereum key',
      notes: 'Creates an ethereum key by connecting to the geth ipc. User name and a key password are required and ' +
        'cannot be left blank or set to string. A valid user is required in the database and more then 1 key can be created' +
        ' for a user. Requires a valid hawk id and key',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        try {
          let balances = [];
          let username = await this.getUsernameFromHawk(request);
          const web3 = new Web3('wss://mainnet.infura.io/ws/v3/01a8db2d6e08450786d39020d7c117a9');
          logInfo({'balance requested from':username});
          let results = await keysModel.find({username:username}).exec();
          for (let item of results) {
            logInfo({'ethereum key results':item});
            balances.push({
              ethereumKey: item.ethereumKey,
              balance: web3.utils.fromWei(await web3.eth.getBalance(item.ethereumKey))
            })
          }
          logInfo({'response':balances});
          return this.formatResponseSuccessDataOrError(true, false, balances, username);
        } catch (e) {
          logInfo(e);
          return await this.formatResponseSuccessDataOrError(false, false,
            Boom.badRequest(e), username)
        }

      }
    }
  }
  static checkBalanceUnencryptedRabbit() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Creates an ethereum key',
      notes: 'Creates an ethereum key by connecting to the geth ipc. User name and a key password are required and ' +
        'cannot be left blank or set to string. A valid user is required in the database and more then 1 key can be created' +
        ' for a user. Requires a valid hawk id and key',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        let username = await this.getUsernameFromHawk(request);
        try {
          await rabbit.sendMasterChannelRabbit({
            username,
            workers: ["coinplot_python_worker_0"],
            command: 'ethCheckBalance'
          });
          let response = await rabbit.getUserMessageFromRabbit(username);
          let balances = [];
          logInfo({'balance requested from':username});
          let results = await keysModel.find({username:username}).exec();
          for (let [index,item] of results.entries()) {
            logInfo({'ethereum key results':item});
            balances.push({
              ethereumKey: item.ethereumKey,
              balance: response[index].toString()
            })
          }
          logInfo({'response':balances});
          response = balances;
          return this.formatResponseSuccessDataOrError(true, false, response, username);
        } catch (e) {
          logInfo(e);
          return await this.formatResponseSuccessDataOrError(false, false,
            Boom.badRequest(e), username)
        }

      }
    }
  }
  static createEthereumKey() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Creates an ethereum key',
      notes: 'Creates an ethereum key by connecting to the geth ipc. User name and a key password are required and ' +
        'cannot be left blank or set to string. A valid user is required in the database and more then 1 key can be created' +
        ' for a user. Requires a valid hawk id and key',
      tags: ['api', 'Ethereum'],
      validate: {
        payload: {
          data: Joi.string().trim().required().disallow("string").disallow(""),
          encrypted: Joi.boolean().required(),
          hmac: Joi.string().trim()
        },
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        let username = await this.getUsernameFromHawk(request);
        try {
          let keypassword = await this.postDecrypt(request);
          await rabbit.sendMasterChannelRabbit({
            username,
            keypassword,
            workers: ["coinplot_python_worker_0"],
            command: 'ethCreateKey'
          });
          let result = await rabbit.getUserMessageFromRabbit(username);
          return await this.formatResponseSuccessDataOrError(true, true, result, username);
        } catch (e) {
          logInfo(e);
          return await this.formatResponseSuccessDataOrError(false, false,
            Boom.badRequest('Hmac Invalid or encryption error'), null)
        }
      }
    }
  }

  static sendEthereumTransaction() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Sends an ethereum transaction',
      notes: 'Sends an ethereum transaction',
      tags: ['api', 'Ethereum'],
      validate: {
        payload: {
          body: Joi.object().keys({
            addressFrom: Joi.string().required(),
            addressTo: Joi.string().required(),
            amountToSend: Joi.string(),
            keyId: Joi.string(),
            gas: Joi.string().default('23000'),
            data: Joi.string().default('default message')
          }),
          encrypted: Joi.boolean().required().default(false),
          hmac: Joi.string().trim()
        },
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        try {
          const web3 = new Web3();
          let encrypted = request.payload.encrypted;
          if (encrypted===true) {
          } else if (encrypted===false) {
            let username = await this.getUsernameFromHawk(request);
            let _id = request.payload.body.keyId;
            let addressFrom = request.payload.body.addressFrom;
            let query = {
              _id:ObjectId(_id),
              username: username,
              ethereumKey: addressFrom
            };
            let result = await keysModel.findOne(query).exec();
            let body = request.payload.body;
            let gasToSpend = body.gas;
            let addressTo = body.addressTo;
            let amountToSend = body.amountToSend;
            let data = body.data;
            let from = body.addressFrom;
            await rabbit.sendMasterChannelRabbit({
              username,
              currentKey:from,
              workers: ["coinplot_python_worker_0"],
              command: 'ethGetTransactionCount'
            });
            let nonce = await rabbit.getUserMessageFromRabbit(username);
            let txConfig = {
              nonce: nonce,
              from: result.ethereumKey,
              to: addressTo,
              value: await web3.utils.toBN(await web3.utils.toWei(amountToSend.toString(), 'ether')),
              data: await web3.utils.toHex(data),
              gas: Number(gasToSpend)
            };
            await rabbit.sendMasterChannelRabbit({
              username,
              txConfig,
              workers: ["coinplot_python_worker_0"],
              command: 'ethSendTransaction'
            });
            let signedTx = await rabbit.getUserMessageFromRabbit(username);
            logInfo(signedTx);
            return await this.formatResponseSuccessDataOrError(true, false, signedTx, username);
          }
        } catch (e) {
          logInfo(e);
          return this.formatResponseSuccessDataOrError(false, false,
              Boom.badRequest(e), null)
        }
      }
    }
  }

  static sendEthereumTransactionUnencryptedOld() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Sends an ethereum transaction',
      notes: 'Sends an ethereum transaction',
      tags: ['api', 'Ethereum'],
      validate: {
        payload: {
          body: Joi.object().keys({
            addressFrom: Joi.string().required(),
            addressTo: Joi.string().required(),
            amountToSend: Joi.string(),
            keyId: Joi.string(),
            gas: Joi.string().default('23000'),
            data: Joi.string().default('default message')
          }),
          encrypted: Joi.boolean().required().default(false),
          hmac: Joi.string().trim()
        },
        query: {
          token: Joi.string()
        }
      },
      handler: async (request, h) => {
        try {
          const web3 = new Web3('wss://mainnet.infura.io/ws/v3/01a8db2d6e08450786d39020d7c117a9');

          const personal = new Web3('/workspace/coinplotethdir0/geth.ipc', net);

          let encrypted = request.payload.encrypted;
          if (encrypted===true) {
          } else if (encrypted===false) {
            let walletTxId = uuid.v4();
            let username = await this.getUsernameFromHawk(request);
            let secretKey = await redis.getSecretKey(username);
            let txDoc = await new ethereumTxModel({
              username,
              uuid:walletTxId
            });
            let _id = request.payload.body.keyId;
            let addressFrom = request.payload.body.addressFrom;
            let query = {
              _id:ObjectId(_id),
              username: username,
              ethereumKey: addressFrom
            };
            console.log('send function query...' + JSON.stringify(query));
            let result = await keysModel.findOne(query).exec();
            let body = request.payload.body;
            let gasToSpend = body.gas;
            let addressTo = body.addressTo;
            let amountToSend = body.amountToSend;
            let data = body.data;
            let nonce = await web3.eth.getTransactionCount(result.ethereumKey);
            let txConfig = {
              nonce: nonce,
              from: result.ethereumKey,
              to: addressTo,
              value: await web3.utils.toBN(await web3.utils.toWei(amountToSend.toString(), 'ether')),
              data: await web3.utils.toHex(data)
            };
            logInfo(txConfig);
            txConfig.gas = Number(gasToSpend);
            txConfig.gasPrice = await web3.eth.getGasPrice();
            logInfo(txConfig);
            console.log('right before personal');
            let signedTx = await personal.eth.personal.signTransaction(txConfig, result.keypassword);
            logInfo(signedTx);
            console.log('right before send');
            let usernameMd5 = await redis.getAsync(username+'_Md5');
            console.log({'user name from inside send': usernameMd5});
            web3.eth.sendSignedTransaction(signedTx.raw)
              .on('transactionHash' ,(hash)=>{
                socket.emit('send_message', format.formatEmitSync(usernameMd5, secretKey, {hash}));
                txDoc.hash = hash;
                txDoc.save();
                logInfo(hash);
              })
              .on('receipt', (receipt)=>{
                txDoc.receipt = receipt;
                txDoc.save();
                socket.emit('send_message', format.formatEmitSync(usernameMd5, secretKey, {receipt}));
                logInfo(receipt);
              })
              .on('confirmation', (confirmationNumber, receipt)=>{
                txDoc.confirmationNumber = confirmationNumber;
                socket.emit('send_message', format.formatEmitSync(usernameMd5, secretKey, {confirmationNumber}));
                txDoc.save();
                logInfo(confirmationNumber);
                logInfo(receipt);
              })
              .on('error', error => {
                if (error.message.includes('-32000')) {
                  error.message = "insufficient funds for gas * price + value"
                }
                socket.emit('send_message', format.formatEmitSync(usernameMd5, secretKey, {error:error.message}));
                txDoc.error = error;
                txDoc.save();
                logInfo(error);
              });
            return await this.formatResponseSuccessDataOrError(true, false, signedTx, username);
         }
      } catch (e) {
          logInfo(e);
          return this.formatResponseSuccessDataOrError(false, false,
            Boom.badRequest(e), null)
        }
      }
    }
  }


  static getLatestBlock() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Latest Block',
      notes: 'Returns the latest block of the geth node only works in full sync mode otherwise will return zero ' +
          ' Requires a valid hawk id and key',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string()
        }
      },
      handler: (request, h) => {
        const web3 = new Web3('wss://mainnet.infura.io/ws/v3/01a8db2d6e08450786d39020d7c117a9');
        const personalWeb3 = new Web3('/workspace/coinplotethdir0/geth.ipc', net);
        return web3.eth.getBlockNumber().then(value => {
          return personalWeb3.eth.getBlockNumber().then(valuePersonal => {
            return {
              infura: value,
              local: valuePersonal
            }
          }).catch(e=>{
            return Boom.badRequest(e);
          });
        }).catch(e=>{
          return Boom.badRequest(e);
        });
      }
    }
  }
  static getSyncing() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Get syncing data',
      notes: 'Returns the latest downloaded blocks of the geth node. Requires a valid hawk id and key',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string()
        }
      },
      handler: (request, h) => {
        const web3 = new Web3('wss://mainnet.infura.io/ws/v3/01a8db2d6e08450786d39020d7c117a9');
        const personal = new Web3('/workspace/coinplotethdir0/geth.ipc', require('net'));
        return web3.eth.isSyncing().then(value=>{
          return personal.eth.isSyncing().then(valueLocal=>{
            return {
              value, valueLocal
            }
          }).catch(e=>{
            return Boom.badRequest(e);
          });
        }).catch(e=>{
          return Boom.badRequest(e);
        });
      }
    }
  }
  static getNodeInfo() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'Latest Node Info',
      notes: 'Returns latest block info, requires a valid hawk id and key',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string()
        }
      },
      handler: (request, h) => {
        const web3 = new Web3('wss://mainnet.infura.io/ws/v3/01a8db2d6e08450786d39020d7c117a9');
        const personalWeb3 = new Web3('/workspace/coinplotethdir0/geth.ipc', require('net'));
        return web3.eth.getBlock('latest').then(valueInfura =>{
          return personalWeb3.eth.getBlock('latest').then(valuePersonal=>{
            return {
              infura: valueInfura,
              local: valuePersonal
            }
          }).catch(e=>{
            return Boom.badRequest(e);
          });
        }).catch(e=>{
          return Boom.badRequest(e);
        });
      }
    }
  }
  static listEthereumKeys() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'List Ethereum Keys for a user',
      notes: 'Returns all ethereum keys for a user, requires a valid hawk id and key, jwt, hmac, encrypted request/response',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string()
        },
      },
      handler: async (request, h) => {
        try {
          let username = await this.getUsernameFromHawk(request);
          let results = await keysModel.find({username:username}).exec();
          return this.formatResponseSuccessDataOrError(true, true, results, username);
        } catch (e) {
          return this.formatResponseSuccessDataOrError(false, false, e, null)
        }
      }
    }
  }
  static listEthereumKeysUnencrypted() {
    return {
      auth: {
        strategy: "hawkjwt"
      },
      description: 'List Ethereum Keys for a user',
      notes: 'Returns all ethereum keys for a user, requires a valid hawk id and key, jwt, hmac, encrypted request/response',
      tags: ['api', 'Ethereum'],
      validate: {
        query: {
          token: Joi.string()
        },
      },
      handler: async (request, h) => {
        try {
          let username = await this.getUsernameFromHawk(request);
          let results = await keysModel.find({username:username}).exec();
          return this.formatResponseSuccessDataOrError(true, false, results, username);
        } catch (e) {
          return this.formatResponseSuccessDataOrError(false, false, e, null)
        }
      }
    }
  }

  static async postDecrypt(request) {
    try {
      let encrypted = request.payload.encrypted;
      let data = request.payload.data;
      let username = await this.getUsernameFromHawk(request);
      let secretKey = await redis.getSecretKey(username);
      if (this.checkHmac(request, username)) {
        console.log({'hmac for create key match': true});
        if (encrypted === true) {
          return CommonEncryption.decrypt(data, secretKey);
        }
      }
    }catch (e) {
      return e
    }
  }


  static async getUsernameFromHawk(request) {
    let hawkKey = request.auth.credentials.hawk.key;
    let query = {
      hawkKey:hawkKey
    };
    let result = await userModel.findOne(query).exec();
    return result.username;
  }
  static async checkHmac(request, username) {
    let hmac = request.payload.hmac;
    let secretKey = await redis.getSecretKey(username);
    let requestPayloadCopy = JSON.parse(JSON.stringify(request.payload));
    delete requestPayloadCopy.hmac;
    let testSig = crypto.createHmac('sha1', secretKey).update(JSON.stringify(requestPayloadCopy)).digest('hex');
    if (testSig!==hmac){
      return true;
    }else {
      return false;
    }
  }
  static async formatResponseSuccessDataOrError(success, encrypted , result, username) {
    if (success === true) {
      if (encrypted===true) {
        let secretKey = await redis.getSecretKey(username);
        let encryptedBody = CommonEncryption.encrypt(result, secretKey);
        const hmac = await this.generateHmac(encryptedBody, username);
        return await this.formatResponse(true, encryptedBody , hmac, username)
      }else {
        const hmac = await this.generateHmac(result, username);
        return await this.formatResponse(false, result, hmac , username);
      }
    }else if(success === false){
      return this.formatFailedResponse(result);
    }else {
      return new Error("No result To format");
    }
  }
  static async formatFailedResponse(error) {
    return {
      success:false,
      error: error
    }
  }
  static async formatResponse(encrypted , data, hmac, username) {
    if(encrypted===true) {
      return {
        success: true,
        encrypted: true,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }else {
      return {
        success: true,
        encrypted:false,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }
  }
  static async formatResponseLock(data) {
    return lock.acquire(this.username, async ()=>{
      return await this.generateHmac(data, this.username)
    }).then(hmac=>{
      return {
        success: true,
        data: data,
        hmac: hmac
      }
    });
  }
  static async generateHmac(body, username) {
    let secretKey = await redis.getSecretKey(username);
    return crypto.createHmac('sha1', secretKey).update(JSON.stringify(body)).digest('hex')
  }
}

function hashCode(s){
  return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);
}

function generateMd5Hash(text) {
  return crypto.createHash('md5').update(text).digest("hex")
}

module.exports = EthereumController;
