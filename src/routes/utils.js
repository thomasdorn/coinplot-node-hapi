

let routes = [
  {
    method: 'GET',
    path: '/api/swagger/hawk/browser.js',
    handler: function (request, reply) {
      return reply.file('src/views/browser.js');
    }
  },
  {
    method: 'GET',
    path: '/api/swagger/timestamp',
    handler: function (request, h) {
      return (Math.trunc(Date.now() / 1000))
    }
  }
];

exports.routes = routes;
