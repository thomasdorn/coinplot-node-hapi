let userController = require('../controllers/userController');
let encryptionController = require('../controllers/encryptionController');

module.exports.routes =  [
  { method: 'GET',    path: '/api/encryption/publickey',  config: userController.getServerPublicKey() },
  { method: 'POST',    path: '/api/encryption/encryptdecrypt',  config: encryptionController.encryptDecryptBody() }
];
