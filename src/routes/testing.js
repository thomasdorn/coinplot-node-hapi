let testingController = require('../controllers/testingController');

module.exports.routes =  [
  { method: 'GET',    path: '/api',  config: testingController.helloWorld() },
  { method: 'GET',    path: '/api/testing/hawk',  config: testingController.testHawk() },
  { method: 'GET',    path: '/api/testing/hawkjwt',  config: testingController.testHawkJwt() },
  { method: 'POST',    path: '/api/testing/hmac',  config: testingController.getHmac() }

];
