let priceDataController = require('../controllers/priceDataController');
let ethereumController = require('../controllers/ethereumController');
let userController = require('../controllers/userController');

module.exports.routes =  [
  { method: 'GET',   path: '/api/user/logout',  config: userController.logoutUser() }
];
