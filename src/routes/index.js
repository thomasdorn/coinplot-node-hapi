let _ = require('lodash');
let reqDir = require('require-directory');
let routes = reqDir(module, __dirname);
const cluster = require('cluster');

// console.log(routes);

let routesFiltered = _.map(routes, route=>{
  return route.routes;
});

routesFiltered = _.flattenDeep(routesFiltered);
if(cluster.isMaster) {
  console.log("Current routes:");
  let routePaths = routesFiltered.map(route=>{
    return route.path
  });
  console.log(routePaths);
}


module.exports = routesFiltered;
