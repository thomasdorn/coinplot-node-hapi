let SwaggerHandler = require('../controllers/swagger');

// let swaggerHandler = new SwaggerHandler();

module.exports.routes = [
  { method: 'GET', path: '/documentation', config: SwaggerHandler.index() },
];
