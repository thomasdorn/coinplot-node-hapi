let priceDataController = require('../controllers/priceDataController');
let ethereumController = require('../controllers/ethereumController');
let userController = require('../controllers/userController');

module.exports.routes =  [
  { method: 'GET',    path: '/api/bitcoin',  config: priceDataController.getBitcoinPrice() },
  { method: 'GET',    path: '/api/ethereum',  config: priceDataController.getEthereumPrice() },
  { method: 'GET',    path: '/api/zcash',  config: priceDataController.getZcashPrice() },
  { method: 'POST',    path: '/api/ethereum/create/key',  config: ethereumController.createEthereumKey() },
  { method: 'GET',    path: '/api/ethereum/node/info',  config: ethereumController.getNodeInfo() },
  { method: 'POST',   path: '/api/user/create',  config: userController.createUser() },
  { method: 'POST',   path: '/api/user/login',  config: userController.loginUser() },
  { method: 'POST',   path: '/api/push/hmac/secretkey',  config: userController.acceptClientSecret() },
  { method: 'POST',   path: '/api/push/hmac/secretkey/unencrypted',  config: userController.acceptClientSecretUnencrypted() },
  { method: 'GET', path: '/api/ethereum/node/latest', config: ethereumController.getLatestBlock() },
  { method: 'GET', path: '/api/ethereum/node/syncing', config: ethereumController.getSyncing() },
  { method: 'GET', path: '/api/user/ethereum/listkeys', config: ethereumController.listEthereumKeys() },
  { method: 'GET', path: '/api/user/ethereum/listkeys/unencrypted', config: ethereumController.listEthereumKeysUnencrypted() },
  { method: 'GET', path: '/api/user/ethereum/checkbalance/unencrypted', config: ethereumController.checkBalanceUnencrypted() },
  { method: 'GET', path: '/api/user/ethereum/checkbalance/rabbit/unencrypted', config: ethereumController.checkBalanceUnencryptedRabbit() },
  { method: 'GET', path: '/api/user/ethereum/getkeyfile/unencrypted', config: ethereumController.getEthereumKeyFile() },
  { method: 'POST', path: '/api/user/ethereum/send/unencrypted',  config: ethereumController.sendEthereumTransaction() },
  { method: 'POST', path: '/api/user/ethereum/send/old/unencrypted',  config: ethereumController.sendEthereumTransactionUnencryptedOld() },
  { method: 'GET', path: '/api/user/java/hello', config: ethereumController.runJarFile() },

];
