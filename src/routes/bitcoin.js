let priceDataController = require('../controllers/priceDataController');
let ethereumController = require('../controllers/ethereumController');
let userController = require('../controllers/userController');
let bitcoinController = require('../controllers/bitcoinController');
let ChartsController = require('../controllers/chartsController');
module.exports.routes =  [
  { method: 'GET',    path: '/api/bitcoin/rabbit/nodeinfo',  config: bitcoinController.getNodeInfo()  },
  { method: 'GET',    path: '/api/bitcoin/getwalletinfo',  config: bitcoinController.getWalletInfo() },
  { method: 'GET',    path: '/api/bitcoin/getblockcount',  config: bitcoinController.getBlockCount() },
  { method: 'GET',    path: '/api/bitcoin/getlistbanned',  config: bitcoinController.getListBanned() },
  { method: 'GET',    path: '/api/bitcoin/price',  config: ChartsController.getBtcPrice() },
  { method: 'GET',    path: '/api/bitcoin/rabbit/createkey',  config: bitcoinController.createKey()  },
  { method: 'GET',    path: '/api/bitcoin/rabbit/balance',  config: bitcoinController.getBtcAccountBalance()  }

];
