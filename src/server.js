'use strict';
const routes = require('./routes/index');
const Hapi = require('hapi');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const freeMem = require('os').freemem();
const HAPIWebSocket = require("hapi-plugin-websocket");
const Ejs = require('ejs');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const inert = require('inert');
const userModel = require('./models/user');
const CommonMongoose = require('./services/mongoose').CommonMongoose;
const envHandle = require('dotenv').config();



let express = require('express');
let CommonIo = require('./services/socketio');

// let environment = 'development';
let environment = 'production';

if (environment === 'development') {
    process.env.NODE_ENV = 'development'
} else if ( environment === 'production' ){
    process.env.NODE_ENV = 'production'
}

try {
    var Pack = require('../package');
    if (process.env.NODE_ENV === 'development') {
        var swaggerOptions = {
            info: {
                title: 'Coinplot.info Documentation - ' + 'Free memory: ' + freeMem,
                version: Pack.version,
            },
            schemes: ['http'],
            grouping: 'tags',
            documentationPage: false,
            // swaggerUI: false
        }
    } else if (process.env.NODE_ENV === 'production') {
        var swaggerOptions = {
            info: {
                title: 'Coinplot.info Documentation - ' + 'Free memory: ' + freeMem,
                version: Pack.version,
            },
            jsonPath: '/api/swagger.json',
            schemes: ['https'],
            grouping: 'tags',
            documentationPage: false,
        }
    }
}catch (e) {
    var Pack = require('./package');
    if (process.env.NODE_ENV === 'development') {
        var swaggerOptions = {
            info: {
                title: 'Coinplot.info Documentation - ' + 'Free memory: ' + freeMem,
                version: Pack.version,
            },
            schemes: ['http'],
            grouping: 'tags',
            documentationPage: false,
            // swaggerUI: false
        }
    } else if (process.env.NODE_ENV === 'production') {
        var swaggerOptions = {
            info: {
                title: 'Coinplot.info Documentation - ' + 'Free memory: ' + freeMem,
                version: Pack.version,
            },
            jsonPath: '/api/swagger.json',
            schemes: ['https'],
            grouping: 'tags',
            documentationPage: false,
        }
    }
}
const params = {
    host: '0.0.0.0',
    port: 8080,
};
const server = Hapi.server(params);
const getCredentialsFunc = function (id) {
    return userModel.find({hawkId:id}).exec().then(result=>{
       if (result.length) {
           return {
               key: result[0].hawkKey,
               algorithm: 'sha256'
           };
       }else {
           console.log("no hawk id found");
           return null;
       }
    });
};
const validateJwt = async function (decoded, request) {
    // do your checks to see if the person is valid
    if (decoded) {
        let result = await userModel.find({_id:decoded.id}).exec();
        if (result.length) {
            return { isValid: true };
        } else {
            return { isValid: false }
        }
    }
    else {
        return { isValid: false };
    }
};
const start = async function () {
    try {
        await server.register(require('hapi-auth-hawk'));
        await server.register(require('hapi-auth-jwt2'));
        await server.register(require('nuisance'));
        server.auth.strategy('hawk', 'hawk', { getCredentialsFunc });
        server.auth.strategy('jwt', 'jwt',
          { key: 'XSBBJCSJ9MAXKRZJCWUFRIFWTWEWGQTMOMISB9QAWKGXVXGQTBEDPDEHOGV9EZINVZYLKTSELCFZ9ODPW',          // Never Share your secret key
              validate: validateJwt,            // validate function defined above
              verifyOptions: { algorithms: [ 'HS256' ] } // pick a strong algorithm
          });
        server.auth.strategy('hawkjwt', 'nuisance', {
            strategies: ['hawk', 'jwt']
        });
        await server.register(inert);
        await server.route(routes);
        await server.register(HAPIWebSocket);
        await server.register(Vision);
        await server.register({
            plugin: HapiSwagger,
            options: swaggerOptions,
        });

        await server.register({
            plugin: require('hapi-cors'),
            options: {
                origins: ['http://localhost:4200']
            }
        });

        await server.views({
            engines: { html: require('handlebars') , ejs: Ejs},
            relativeTo: __dirname,
            path: './views/',
            partialsPath: './views/withPartials',
            helpersPath: './views/helpers',
        });
        CommonMongoose.classConnect();
        console.log({
            cpu:process.pid,
            env:JSON.stringify(process.env.NODE_ENV)
        });
        if (cluster.isMaster) {

            let streamService = require('./services/streamService');
            streamService.establishServices();
            // console.log(`Master ${process.pid} is running`);
            for (let i = 0; i < numCPUs; i++) {
                cluster.fork();
            }
            cluster.on('exit', (worker, code, signal) => {
                console.log(`worker ${worker.process.pid} died`);
            });
        } else {
            await server.start();
            console.log({'Server running at': server.info.uri});
            console.log({'Worker `${process.pid}` started': true});
        }
    } catch(err) {
        console.log(err);
    }
};
start();
process.on('unhandledRejection', (err) => {
    throw err;
});


