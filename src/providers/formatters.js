const _ = require('lodash');
const CommonEncrypted = require('../providers/encryptionProvider');
const redis = require('../services/redis');
const crypto = require('crypto');

let format = {};
format.formatEmit = async function(username, unencryptedDataObject ) {
  let secretKey = await redis.getSecretKey(username);
  let encryptedDataString = CommonEncrypted.encrypt(unencryptedDataObject, secretKey);
  return {
    username: encryptedDataString
  }
};

format.formatEmitSync = function(username, secretKey, unencryptedDataObject ) {
  let encryptedDataString = CommonEncrypted.encrypt(unencryptedDataObject, secretKey);
    return {
      username: username, data: encryptedDataString
    }
};

format.filterResponse = function(success, toDisplay, error, message, data) {
  if (toDisplay === 'all') {
    return formatResponse(success, {
      error,
      message,
      data
    });
  }else {
    let response = _.filter(data, function (o) {
      if (toDisplay.includes(o.type)) {
        return o;
      }
    });
    if (message === null) {
      return formatResponse(success, {response})
    }else {
      return formatResponse(success, {
        error,
        message,
        response
      });
    }
  }
}
function formatResponse(success, response) {
  if (success) {
    return {
      success,
      data: response
    }
  }
};
module.exports = format;
