let cache = require('../services/node_cache');
let redis = require('../services/redis');


class PriceDataProvider {
  constructor(cache,
              redis
  ){

  }

  async getPriceData(coinName) {
    console.log("returning: " + coinName);
    try{
      let result = await redis.getValue(coinName);
      return result;
    }catch (e) {
      console.log(e);
      return e;
    }
  }
}

module.exports = new PriceDataProvider();
