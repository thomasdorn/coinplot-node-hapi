class RabbitMQService {
  constructor(props) {
    this.rabbit = require('amqplib')
  }

  getUserMessageFromRabbit(username) {
    return new Promise((resolve,reject)=> {
      return this.rabbit.connect('amqp://celeryuser:celery@dornhost.com:5672/celeryvhost').then(function (conn) {
        return conn.createChannel().then(function (ch) {
          let ok = ch.assertQueue(username, {durable: false});
          return ok.then(function (_ok) {
            return ch.consume(username, function (msg) {
              console.log(" [x] Received '%s'", msg.content.toString());
              resolve(JSON.parse(msg.content.toString()));
              return conn.close();
            }, {noAck: true});
          });
        });
      })
    })
  };

  sendMasterChannelRabbit(body) {
    let worker;
    if(body.workers) {
      [worker] = body.workers
    }
    worker = "celeryvhost";
    return new Promise((resolve, reject) => {
      return this.rabbit.connect('amqp://celeryuser:celery@dornhost.com:5672/'+worker).then(function(conn) {
        return conn.createChannel().then(function(ch) {
          let q = 'master';
          let ok = ch.assertQueue(q, {durable: false});
          return ok.then(function(_qok) {
            let message = JSON.stringify(body);
            ch.sendToQueue(q, Buffer.from(message));
            console.log(" [x] Sent '%s'", message);
            resolve();
            return ch.close();
          });
        })
      })
    })
  };
}
module.exports = new RabbitMQService();