let userModel = require('../models/user');
let redis = require('../services/redis');
let crypto = require('crypto');
let CommonEncryption = require('../providers/encryptionProvider');


class Response {
  static async postDecrypt(request) {
    try {
      let encrypted = request.payload.encrypted;
      let data = request.payload.data;
      let username = await this.getUsernameFromHawk(request);
      let secretKey = await redis.getSecretKey(username);
      if (this.checkHmac(request, username)) {
        console.log({'hmac for create key match': true});
        if (encrypted === true) {
          return CommonEncryption.decrypt(data, secretKey);
        }
      }
    }catch (e) {
      return e
    }
  }
  static async getUsernameFromHawk(request) {
    let hawkKey = request.auth.credentials.hawk.key;
    let query = {
      hawkKey:hawkKey
    };
    let result = await userModel.findOne(query).exec();
    return result.username;
  }
  static async checkHmac(request, username) {
    let hmac = request.payload.hmac;
    let secretKey = await redis.getSecretKey(username);
    let requestPayloadCopy = JSON.parse(JSON.stringify(request.payload));
    delete requestPayloadCopy.hmac;
    let testSig = crypto.createHmac('sha1', secretKey).update(JSON.stringify(requestPayloadCopy)).digest('hex');
    if (testSig!==hmac){
      return true;
    }else {
      return false;
    }
  }
  static async formatResponseSuccessDataOrError(success, encrypted , result, username) {
    if (success === true) {
      if (encrypted===true) {
        let secretKey = await redis.getSecretKey(username);
        let encryptedBody = CommonEncryption.encrypt(result, secretKey);
        const hmac = await this.generateHmac(encryptedBody, username);
        return await this.formatResponse(true, encryptedBody , hmac, username)
      }else {
        const hmac = await this.generateHmac(result, username);
        return await this.formatResponse(false, result, hmac , username);
      }
    }else if(success === false){
      return this.formatFailedResponse(result);
    }else {
      return new Error("No result To format");
    }
  }
  static async formatFailedResponse(error) {
    return {
      success:false,
      error: error
    }
  }
  static async formatResponse(encrypted , data, hmac, username) {
    if(encrypted===true) {
      return {
        success: true,
        encrypted: true,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }else {
      return {
        success: true,
        encrypted:false,
        data: data,
        hmac: username===null? undefined : hmac
      }
    }
  }
  static async formatResponseLock(data) {
    return lock.acquire(this.username, async ()=>{
      return await this.generateHmac(data, this.username)
    }).then(hmac=>{
      return {
        success: true,
        data: data,
        hmac: hmac
      }
    });
  }
  static async generateHmac(body, username) {
    let secretKey = await redis.getSecretKey(username);
    return crypto.createHmac('sha1', secretKey).update(JSON.stringify(body)).digest('hex')
  }
}

module.exports = Response;