let CryptoJS = require('crypto-js');

class CommonEncryption {
  static encrypt(body, secretKey) {
    return CryptoJS.AES.encrypt(JSON.stringify(body), secretKey).toString()
  }
  static decrypt(body, secretKey) {
    let decrypted = CryptoJS.AES.decrypt(body.toString(), secretKey).toString(CryptoJS.enc.Utf8);
    if (typeof decrypted === 'string' || decrypted instanceof String){
      console.log('inside decrypt string check '+ decrypted);
      return decrypted;
    }else {
      return JSON.parse(decrypted);
    }
  }
}

module.exports = CommonEncryption;
