module.exports = function (io, socket) {
  socket.on('geth message', (data) => {
    io.emit('message', data);
    io.emit('chat message', data);
  });
  socket.on('link message', data => {
    io.emit('ai message', data);
  });
  socket.on('link', data => {
    io.emit('link', data);
  });
  socket.on('message', data => {
    io.emit('message', data);
    io.emit('chat message', data);
  });
  socket.on('geth', data => {
    io.emit('chat', data);
  });
  socket.on('send_message', data => {
    io.emit('send_message', data);
  });
  socket.on('cpu_message', data => {
    io.emit('cpu_message', data);
  });
  socket.on('heart_beat', data => {
    console.log(data)
  });
  socket.on('btc message', data => {
    io.emit('btc message', data);
  });
};
