const mongoose = require('mongoose');
const logger = require('./logger');

let deps = [
  mongoose,
  logger
];

class CommonMongoose {
  constructor(mongoose,
              logger

  ){
    this.logger = logger;
    this.connectionString = 'mongodb://nodeapplication:AoIBAQDQrYa7w9GN3tMI@coinplot.info:27018/node-hapi-api';
    this.localConnectionString = 'mongodb://mongo:27017/node-hapi-api';
    this.log = (info) => {
      this.logger.setModuleAndLog(module, info);
    };
    this.options = {};
    if (process.env.NODE_ENV === 'production') {
        this.options = {
        useCreateIndex: true,
        useNewUrlParser: true,
        ssl: false
      }
    } else if (!process.env.NODE_ENV) {
      this.options = {
        useCreateIndex: true,
        useNewUrlParser: true,
        ssl: false
      }
    } else if (process.env.ENVIRONMENT === 'development') {
      this.options = {
        useCreateIndex: true,
        useNewUrlParser: true,
        ssl: false
      }
    }
    this.mongoose = mongoose;
    this.connect = mongoose.connect;
    // console.log({
    //   'mongoose init': this.options,
    //   'connectionString': this.connectionString,
    //   'localConnectionString':this.localConnectionString
    // });
  }
  classConnect() {
    if (process.env.ENVIRONMENT === 'production') {
      console.log('environment connection string', this.connectionString);
      this.connect(this.connectionString, this.options);
    } else {
      console.log('environment connection string', this.localConnectionString);
      this.connect(this.localConnectionString, this.options);
    }
    this.db = this.mongoose.connection;
    this.db.on('error', console.error.bind(console, 'connection error:'));
    this.db.on('connected', ()=>{ this.log('connected to db')});
  }
}




module.exports = mongoose;
module.exports['CommonMongoose'] = new CommonMongoose(...deps);
