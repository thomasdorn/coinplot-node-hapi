const openpgp = require('openpgp');
const generate = require('iota-generate-seed');
let crypto = require('crypto');

class OpenPGP {
  constructor() {
    this.privkey = '';
    this.pubkey = '';
    this.revocationCertificate = '';
    this.options = {
      userIds: [{ name:'coinplot.info', email:'admin@coinplot.info' }], // multiple user IDs
      numBits: 4096,                                            // RSA key size
      passphrase: generateHash()       // protects the private key
    };
    openpgp.generateKey(this.options).then(function(key) {
      this.privkey = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
      this.pubkey = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
      this.revocationCertificate = key.revocationCertificate; // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
    });
  }
  getPublicKey() {
    return this.pubkey;
  }
}

function generateHash() {
  let offset = (new Date).getTime()*Math.random();
  let iotaSeed = generate();
  let toHash = (offset+iotaSeed);
  let sha = crypto.createHash('sha1').update(toHash);
  return sha.digest('base64');
}

// module.exports = new OpenPGP();
