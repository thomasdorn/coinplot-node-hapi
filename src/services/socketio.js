const socketIo = require('socket.io');


class CommonSocketIo extends socketIo {
  constructor(server){
    let socketIo = super(server);
    this.io = socketIo;
  }
  getIo() {
    return this.io;
  }
  setApp(app){
    this.io = app;
  }
}

module.exports = CommonSocketIo;
