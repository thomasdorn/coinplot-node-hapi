let osUtils = require('os-utils');
let format = require('../providers/formatters');
let userModel = require('../models/user');
let redis = require('./redis');
class StreamService {
  constructor() {
    this.socket = require('socket.io-client')('https://http2.coinplot.info');
  }
  establishServices() {
    let intervalFunc = async function(parent, parentdBAccessor, parentRedisAccessor){
      let numUsers = await parentdBAccessor.find({$and: [{isLoggedIn: {$exists: true}}, {isLoggedIn: true}]}).countDocuments();
      if(numUsers) {
        let usersLoggedIn = await parentdBAccessor.find({$and: [{isLoggedIn: {$exists: true}}, {isLoggedIn: true}]}).exec();
        if (usersLoggedIn) {
          console.log("emitting to users: " + usersLoggedIn.map(v=>v.username));
        }
        for (let user of usersLoggedIn) {
          osUtils.cpuUsage((async v=>{
            v = (v*100).toString();
            let usernameMd5 = await parentRedisAccessor.getAsync(user.username+'_Md5');
            try {
              parent.pushMessage_Cpu(usernameMd5, await redis.getSecretKey(user.username), {cpuValue:v});
            } catch (e) {
              console.log(e.message.substring(0, 100));
            }
          }));
        }
      }
    };
    setInterval(intervalFunc.bind(null, this, userModel, redis), 620 );
  }
  pushMessage_Cpu(username, secretKey, body) {
    try{
      this.socket.emit('cpu_message', format.formatEmitSync(username, secretKey, body));
    }catch (e) {
    }
  }
}


module.exports = new StreamService();
