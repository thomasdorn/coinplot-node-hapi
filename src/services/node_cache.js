const Node_cache = require( "node-cache" );
const util = require('util');

let deps = [
];

module.exports = new (class CommonCache extends Node_cache {
  constructor() {
    super({
      stdTTL: 10800,
      checkperiod: 10
    });
    // this.Logger = Logger;
    // this.log = function(info) {
    //   this.Logger.setModuleAndLog(module, info);
    // };
    this.cacheGetAsync = util.promisify(this.get);
    this.setCacheAsync = util.promisify(this.set);
    this.on('expired', (key, value)=>{
      console.log(value + ' removed from cache for key: ' + key);
    });
  }
})(...deps);
