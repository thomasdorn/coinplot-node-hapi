const winston = require('winston');
const path = require('path');
const colors = require('colors');
let deps = [
  winston,
  path
];
module.exports = new (class Logger {
  constructor(
    winston,
    path
  ) {
    this.path = path;
    this.logger = winston.createLogger({
      level: 'info',
      format: winston.format.json(),
      defaultMeta: {service: 'HAPI api'},
      transports: [
        new winston.transports.File({filename: 'error.log', level: 'error'}),
        new winston.transports.File({filename: 'combined.log'}),
        new winston.transports.Console()
      ]
    });

    if (process.env.NODE_ENV !== 'production') {
      this.logger.add(new winston.transports.Console({
        format: winston.format.simple()
      }));
    }

  }


  getLabel(callingModule) {
    let parts = callingModule.filename.split(this.path.sep);
    return this.path.join(parts[parts.length - 2], parts.pop());
  }


  setModuleAndLog(callingModule, info) {
    if (typeof info === Object) {
      console.log(colors.blue(JSON.stringify(info)));
      this.logger.info(colors.white({level:'info',nodefile:this.getLabel(callingModule),message:JSON.stringify(info)}));
    } else {
      console.log(colors.blue(info));
      this.logger.info(colors.white({level:'info',nodefile:this.getLabel(callingModule),message:info}));
    }
  }
})(...deps);
