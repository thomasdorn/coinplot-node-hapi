const redis = require('redis');
const generate = require('iota-generate-seed');
const crypto = require('crypto');
const openpgp = require('openpgp');
const cluster = require('cluster');
const promisify = require('util').promisify;
const EventEmitter = require('events');

class CommonRedis extends EventEmitter {
  constructor(connection){
    super();
    this.redis = redis;
    this.client = this.redis.createClient(connection);
    this.client.select(1);
    this.client.on('error', err=>{
      console.log(err);
    });
    this.redisHash = 'BCAAJBQJcrja2AhsMAAoJEAUFPzy3QWS7KeAD_';
    console.log({'redis connecting': this.redisHash});
    this.getAsync = promisify(this.client.get).bind(this.client);
    this.setAsync = promisify(this.client.set).bind(this.client);
    this.on('updateKeys', ()=>{
      this.setEncryptionKeys()
    })
  }

  getRedisHash(){
    return this.generateHash()
  }

  setRedisHash(hash) {
    this.redisHash = hash;
  }

  setEncryptionKeyUpdateInterval() {
    this.emit('updateKeys');
    setInterval(()=>{
      this.emit('updateKeys');
    }, 1000 * 300);
    return this;
  }

  async setValue(key, value) {
    try{
      await this.setAsync(this.redisHash + key, JSON.stringify(value));
    }catch (e) {
      console.log(e);
      return e
    }
  }
  async getValue(key) {
    try{
      return JSON.parse(await this.getAsync(this.redisHash + key));
    }catch (e) {
      console.log(e);
      return e;
    }
  }

  async setSecretKey(username, secretKey) {
    let redisKey = this.redisHash + username + '_secretkey';
    console.log({'redis - secret key': secretKey});
    console.log({'redis - username': username});
    try{
      await this.setAsync(redisKey, JSON.stringify(secretKey));
    }catch (e) {
      console.log(e);
      throw e
    }
  }

  async getSecretKey(username) {
    let redisKey = this.redisHash + username + '_secretkey';
    try{
      return JSON.parse(await this.getAsync(redisKey));
    }catch (e) {
      console.log(e);
      throw e;
    }
  }

  async getPrivateKeyPassword() {
    let redisKey = this.redisHash + 'server_privateKeyPassword';
    try{
      return JSON.parse(await this.getAsync(redisKey));
    }catch (e) {
      console.log(e);
      throw e;
    }
  }

  async setEncryptionKeys() {
    if (cluster.isMaster) {
      let options = {
        userIds: [{ name:'coinplot.info', email:'admin@coinplot.info' }], // multiple user IDs
        numBits: 512,                                            // RSA key size
        passphrase: this.generateHash()       // protects the private key
      };
      const encryptionKeys = await openpgp.generateKey(options);
      try{
        await this.setAsync(this.redisHash + 'server_privateKeyPassword', JSON.stringify(options.passphrase));
        await this.setAsync(this.redisHash + 'server_encryptionkeys', JSON.stringify(encryptionKeys));
        this.emit('finished', encryptionKeys.publicKeyArmored);
      }catch (e) {
        console.log(e);
        throw e;
      }
    }
  }

  async getEncryptionKeys() {
    let redisKey = this.redisHash + 'server'+ '_encryptionkeys';
    try {
      return JSON.parse(await this.getAsync(redisKey));
    }catch (e) {
      console.log(e);
      throw e;
    }
  }
  async getPrivateKey() {
    let redisKey = this.redisHash + 'server'+ '_encryptionkeys';
    try {
      return (JSON.parse(await this.getAsync(redisKey))).privateKeyArmored;
    }catch (e) {
      console.log(e);
      throw e;
    }
  }
  async setPublicKey(username, secretKey) {
    let redisKey = this.redisHash + username + '_secretkey';
    try{
      await this.setAsync(redisKey, JSON.stringify(secretKey));
    }catch (e) {
      console.log(e);
      throw e
    }
  }
  async getPublicKey(username) {
    let redisKey = this.redisHash + username + '_secretkey';
    try{
      return JSON.parse(await this.getAsync(redisKey));
    }catch (e) {
      console.log(e);
      throw e;
    }
  }
  generateHash() {
    let offset = (new Date).getTime()*Math.random();
    let iotaSeed = generate();
    let toHash = (offset+iotaSeed).shuffle();
    let sha = crypto.createHash('sha1').update(toHash);
    return sha.digest('base64');
  }
}
String.prototype.shuffle = function () {
  let a = this.split(""),
    n = a.length;
  for(let i = n - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }
  return a.join("");
};

module.exports = new CommonRedis('redis://redisserver:6379');

