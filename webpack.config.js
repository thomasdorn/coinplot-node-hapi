// const path = require('path');
// module.exports = {
//   entry: './src/server.js',
//   devtool: 'inline-source-map',
//   target: "node",
//   module: {
//     rules: [
//       {
//         test: /\.tsx?$/,
//         use: 'ts-loader',
//         include: ['/node_modules/']
//         // exclude: /node_modules/
//       }
//     ]
//   },
//   resolve: {
//     extensions: [ '.js' ]
//   },
//   output: {
//     filename: 'bundle.js',
//     path: '/run/media/thomas/external/coinplot-node-hapi-api/dist'
//   }
// };
module.exports = {
  target: "node",
  entry: "./src/server.js",
  output: {
    filename: "./bundle.js"
  }
};